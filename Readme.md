# Ressources du Mooc Recherche reproductible / Reproducible research Mooc resources

[English version below]

Cet entrepôt contient toutes les ressources du Mooc "Recherche reproductible : principes méthodologiques pour une science transparente" au format orgmode ou markdown.

Sur [cette page](https://learninglab.gitlabpages.inria.fr/mooc-rr/mooc-rr-ressources/), vous trouverez les liens vers les pages html générées automatiquement ainsi que les liens vers FUN (session 02)

-----------------------------------------------------------------
This repository contains all the resources of the Mooc "Reproducible Research: Methodological Principles for Transparent Science" in orgmode or markdown format.

On[this page] (https://learninglab.gitlabpages.inria.fr/mooc-rr/mooc-rr-ressources/), you will find links to automatically generated html pages as well as links to FUN (session 02)