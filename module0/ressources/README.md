Le texte se divise en quatre fichiers correspondant à trois chapitres, précédés d'une introduction (numérotée 0intro.md). L'idée serait de les placer dans Posons le décor / La reproductibilité, en crise? par exemple en rajoutant quatre "onglets EdX" à l'onglet "vidéo : la reproductibilité, en crise ?".

* Le premier chapitre (1complexe.md) présente la notion de reproductibilité telle qu'elle a été abordée en histoire des sciences. On y introduit les notions de tacit knowledge, virtual witness, litterary technology, experimenter's regress.
* Le deuxième chapitre (2multiforme.md) aborde six catégories de reproductibilité différentes selon les domaines scientifiques selon la typologie de Leonelli et conclue sur le problème de la reproductibilité vue comme un "gold standard" global.  
* Le troisième chapitre (4crise.md) s'intéresse à la narration de "crise" de la reproductibilité.  

Edit :  il manque **une version en anglais**

