#+TITLE: Interview with François Pellegrini: reproducible research and use of personal data
#+Date: <déc 2019>
#+Author: François Pellegrini
#+LANGUAGE: en
** Biography
   :PROPERTIES:
   :CUSTOM_ID: biography
   :END:

*François Pellegrini*, is a computer scientist, professor at the
University of Bordeaux, and a researcher at both the Bordeaux computer
science research laboratory (LaBRI) and at Inria. He is the author of
[[https://gforge.inria.fr/projects/scotch/][Scotch]], a generalist
partitioning program. He is a member of the French National Commission
on Informatics and liberties (CNIL).

** Interview
   :PROPERTIES:
   :CUSTOM_ID: interview
   :END:

*** How can we reconcile the transparency inherent to reproducible research and the respect of confidentiality constraints when working with personal data?
    :PROPERTIES:
    :CUSTOM_ID: how-can-we-reconcile-the-transparency-inherent-to-reproducible-research-and-the-respect-of-confidentiality-constraints-when-working-with-personal-data
    :END:

In accordance with the General Data Protection Regulation, the
individual acquiring the personal data acquires the status of "*data
controller*", making them the "guardian" of the data in question. They
are subject to a *principle of* "*accountability*" with regard to the
methods and procedures implemented in order to administer the processing
in question.

As such, *unless the individuals give express consent when the data is
collected*, they may not make this data free to access to unknown
individuals looking to reproduce the research.

Instead, access must be granted *on a case by case basis*, with
non-disclosure clauses in place, in order to ensure that the scientists
in question are able to process the data on the premises of the data
controller (or, at the very least, within the European Union) in a way
that is compatible with the purposes for which it was initially
collected.

As such, they may (attempt to) reproduce the results, but they may not
carry out other research that differs too much from the research for
which the initial consent has been obtained.

In cases involving *non personal data*, the situation is pretty much the
same, only without the need to strictly adhere to the "Informatics and
Liberties" law.

*** As a reader of scientific papers, what criteria can I use to evaluate the robustness of the data compilation protocol if the data is confidential?
    :PROPERTIES:
    :CUSTOM_ID: as-a-reader-of-scientific-papers-what-criteria-can-i-use-to-evaluate-the-robustness-of-the-data-compilation-protocol-if-the-data-is-confidential
    :END:

*Process and data are two separate things*.

You can *audit a process without /actually/ looking at the data used in
it*.

It is also possible to *test processes using synthetic data* in order to
verify the compliance of the expected results in relation to the
characteristics of the data sets which were generated.

** Additional resources
   :PROPERTIES:
   :CUSTOM_ID: additional-resources
   :END:

The CNIL website:
[[https://www.cnil.fr/en/general-data-protection-regulation-guide-assist-processors]["Understanding
the GDPR"]]

Benjamin Nguyen 2019. 'Anonymization Techniques: Theory and Practice'.
presented at the Cinquième École d'Hiver é-EGC, on the theme of "Privacy
Preserving, Reasoning, Explaining", Metz, 21st January.
[[https://egc2019.sciencesconf.org/resource/page/id/12]].

Gustav Nilsonne. 2019. 'Hack- Finding Value in Anonymized Data:
Exploring Scenarios for Secondary Use', June. [[https://osf.io/2n8b6/]].
