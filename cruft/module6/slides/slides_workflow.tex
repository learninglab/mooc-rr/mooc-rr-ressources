\documentclass[aspectratio=169]{beamer}

\usetheme{Madrid}
\usecolortheme{crane}

\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{textpos}
\usepackage{multirow}
\usepackage{graphicx}

\definecolor{darkgreen}{rgb}{0.0, 0.3, 0.13}

\newcommand{\bluehref}[2]{\href{#1}{\textcolor{blue}{#2}}}

\definecolor{emph}{RGB}{10,150,10}
\definecolor{contrast}{RGB}{0,0,120}
\definecolor{problem}{RGB}{120,0,0}
\definecolor{nice}{RGB}{10,150,10}

\usepackage{listings,bera}
\definecolor{sourcecode}{RGB}{0,0,100}
\definecolor{keywords}{RGB}{200,90,0}
\definecolor{comments}{RGB}{60,179,113}


\title{Automating computations:\\ an introduction to workflows}
% \author[Konrad HINSEN]{Konrad HINSEN}
% \institute[CBM/SOLEIL]
%           {Centre de Biophysique Moléculaire, Orléans, France\\
%            and\\
%            Synchrotron SOLEIL, Saint Aubin, France}
\date{}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Automation is the key to reproducibility}

  \begin{itemize}
  \item \textbf{Interactive work is not reproducible.}
  \item Even if you take notes carefully, you will make mistakes.
  \item Even if you don't make mistakes, you can't be \textit{sure} of it.
  \item \textbf{You know exactly what you did \\ only if you can re-run your computation.}
  \end{itemize}

\end{frame}

\begin{frame}{Programs, scripts, notebooks, workflows}

  \begin{itemize}
  \item Variants of the same idea: \textbf{automation of computations}
  \item Different terms and tools at different levels of organization
  \item \textbf{Program:} data structures, complex algorithms, reusable
  \item \textbf{Script:} simple algorithms, no data abstractions, situated
  \item \textbf{Notebook:} script with comments and output
  \item \textbf{Workflow:} coordination of multiple programs/scripts, \\
                           multiple computers, multiple datasets
  \end{itemize}

\end{frame}

\begin{frame}{Overview of this module}

  \begin{enumerate}
  \item Why workflows?
  \item How to choose a workflow manager?
  \item De \texttt{Make} à \textt{Snakemake}
  \item Case study: the incidence of influenza-like illness revisited
  \item Continuous integration testing: a special kind of workflow
  \end{enumerate}

\end{frame}

\section*{Why workflows?}

\begin{frame}{}

  \begin{center}
  \Large Why workflows?
  \end{center}

\end{frame}

\begin{frame}{Scripts and notebooks don't scale}

  \begin{block}{Code size}
    \begin{itemize}
    \item Linear structure: Hard to read/edit when too long
    \item Solution: decompose into smaller scripts\\
          that are run from a workflow
    \end{itemize}
  \end{block}

  \pause

  \begin{block}{Execution time}
    \begin{itemize}
    \item Example: parameter scan over 100 values
    \item Now add one more value... and run everything again!
    \item Solution: write script for one value,\\
          do the parameter scan in a workflow \\
          where no script is run twice with the same input.
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}{Tasks: the building blocks of workflows}

  \begin{itemize}
  \item Task = step in a computation
  \item Code + input data $\rightarrow$ output data
  \item Often implemented as a script (bash, Python, R, ...)
  \item Input and output data stored in files
  \end{itemize}

\end{frame}

\begin{frame}{Workflows: execution plans for tasks}

  \begin{itemize}
  \item One task's output is another task's input
  \item Simple control structures, e.g. parameter scans
  \item Defined in a text file (``workflow language'')
        or visually as a graph
  \end{itemize}

  \pause
  \vspace{1cm}

  \includegraphics[width=15cm]{workflow-graph.png}

\end{frame}

\begin{frame}{Workflow manager}

  Software that orchestrates the execution of a workflow:
  \begin{enumerate}
  \item Analyze the workflow for data dependencies; \\
        never run a task before its input is available
  \item Decide which tasks must be run, in which order.
  \item Avoid running a task whose output already exists
  \item Run tasks in parallel if possible; \\
        optimize for available computing resources
  \item Deal with failures (crash, power outage ...)
  \end{enumerate}

  \vspace{2mm}
  \textcolor{problem}{There are many different workflow managers,
             and workflows are not portable between them!}

\end{frame}

\section*{How to choose a workflow manager}

\begin{frame}{}

  \begin{center}
  \Large How to choose a workflow manager
  \end{center}

\end{frame}

\begin{frame}{The messy world of workflows}

  \begin{itemize}
  \item Dozens of widely used workflow managers
  \item Hundreds of lesser used ones
  \item The idea has been rediscovered independently many times
  \item Cover widely varying needs
  \item Workflows are not portable between workflow managers\\
        but standards might emerge (e.g. Common Workflow Language)
  \end{itemize}

\end{frame}

\begin{frame}{Where are the tasks run?}

  \begin{itemize}
  \item Personal computer
  \item Remote computer with command-line access
  \item Grid, cloud
  \end{itemize}

\end{frame}

\begin{frame}{Where is the workflow managed?}

  \begin{itemize}
  \item Personal computer
  \item Server (Web interface)
  \end{itemize}

\end{frame}

\begin{frame}{Where is the data stored?}

  \begin{itemize}
  \item Local file on the machine running the task
  \item Network-accessible storage
  \item Cloud storage
  \item Web ressources
  \end{itemize}

\end{frame}

\begin{frame}{How is the computation in a task defined?}

  \begin{itemize}
  \item Compiled executable
  \item Script (Python, R, ...)
  \item Container
  \item Web service (REST API, ...)
  \end{itemize}

\end{frame}

\begin{frame}{How is the workflow defined and updated?}

  \begin{itemize}
  \item Domain-specific language
  \item General-purpose language with libraries or extensions
  \item Visual manipulation of a task graph
  \end{itemize}

\end{frame}

\begin{frame}{Which control flow features are available?}

  \begin{itemize}
  \item Data dependencies
  \item Parameter scans (loops)
  \item Dynamic task graph
  \end{itemize}

\end{frame}

\begin{frame}{How is reproducibility supported?}

  \begin{itemize}
  \item Reproducible software environments
  \item Versioning of the workflow
  \item Provenance tracking for results
  \item Provenance tracking for input data
  \end{itemize}

\end{frame}

\section*{De \texttt{Make} à \textt{Snakemake}}

\begin{frame}{}

  \begin{center}
  \Large De \texttt{Make} à \textt{Snakemake}
  \end{center}

\end{frame}

\begin{frame}{Make: workflows for building software}

  \begin{itemize}
  \item Use case: automating software builds:
    \begin{itemize}
    \item more reliable
    \item faster
    \end{itemize}
  \item Typical tasks: preprocess, compile, link
  \item Developed as part of Unix
  \item Published in 1976
  \item Still widely used for software development
  \end{itemize}

\end{frame}

\begin{frame}[fragile]{Makefiles}

  \begin{block}{Basic}
    \begin{lstlisting}
    program: module-1.c module-2.c
            gcc -o program module1.c module2.c
    \end{lstlisting}
  \end{block}

\end{frame}

\begin{frame}[fragile]{Makefiles}

  \begin{block}{Standard}
    \begin{lstlisting}
      program: module-1.o module-2.o
              gcc -o program module-1.o module-2.o

      module-1.o: module-1.c
              gcc module-1.c

      module-2.o: module-2.c
              gcc module-2.c
    \end{lstlisting}
  \end{block}

\end{frame}

\begin{frame}[fragile]{Makefiles}

  \begin{block}{Advanced}
    \begin{lstlisting}
      program: module-1.o module-2.o
              gcc -o program module-1.o module-2.o

      %.o: %.c
              gcc $*.c
    \end{lstlisting}
  \end{block}

\end{frame}

\begin{frame}{Software build tools beyond \texttt{Make}}

  \begin{itemize}
  \item GNU Build System (Make + autoconf, automake, libtool)
  \item CMake
  \item SCons
  \item Ant, Maven, Gradle
  \item Bazel
  \item ...
  \end{itemize}

\end{frame}

\begin{frame}{Software build tools in scientific computing}

  \begin{itemize}
  \item Well-known and well-documented workflow tools
  \item Designed for
    \begin{itemize}
    \item a single computer defining the software environment
    \item data in local files
    \item tasks implemented by command line tools
    \end{itemize}
  \item No management of software environments
  \end{itemize}

\end{frame}

\begin{frame}{Snakemake: Make for scientific computing}

  \begin{itemize}
  \item Same principle as \texttt{make}: rules, commands, files, modification times
  \item More explicit workflow language, inspired by Python
  \item \texttt{Makefile} $\to$ \texttt{Snakefile}
  \item Support for archived software environments
  \item Support for scripts in Python or R with access to workflow parameters
  \item Can execute multiple tasks in parallel
  \item Support for clusters, grids, and the cloud
  \end{itemize}

\end{frame}

\section*{Case study: the incidence of influenza-like illness revisited}

\begin{frame}{}

\end{frame}

\begin{frame}{}

\end{frame}


\end{document}
