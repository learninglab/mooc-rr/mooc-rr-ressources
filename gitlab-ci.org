* GitLab CI configuration
Here is the manually filtered list of org and md files that need to be
exported to html with the gitlab pages CI mechanism. Have a look at
file:html_src_files.lst to know which files are exported.

Here is the yaml file used for CI. All the work is done in a single
perl script.
#+BEGIN_SRC yaml :tangle .gitlab-ci.yml
image: brospars/pandoc-gitlab-ci:latest
pages:
  stage: deploy
  script:
  - pandoc --version
  # - pandoc --help
  # - pandoc --list-input-formats    # Broken as pandoc dates from 2013 on this image! :(
  - bin/pandoc_fixer.pl html_src_files.lst;
  - for file in `cat html_src_files.lst | sed 's/#.*//g' `; do
        mkdir -p public/`dirname ${file}`;
        mv ${file%.*}.html public/`dirname ${file}`/;
    done
  - cd module2/ressources/; tar zcf rr_org_archive.tgz rr_org/init.el rr_org/journal.org rr_org/init.org ; cd ../.. ; mv module2/ressources/rr_org_archive.tgz public/module2/ressources/
  - cd module2/ressources/; tar zcf replicable_article.tgz replicable_article/Makefile replicable_article/article.org replicable_article/biblio.bib ; cd ../.. ; mv module2/ressources/replicable_article.tgz public/module2/ressources/
  artifacts:
    paths:
    - public
  only:
  - master
#+END_SRC

* Update script =html_src_files.lst=
Once the content of the MOOC has been exported from FUN into =/tmp/course/=:
#+begin_src shell :results output :exports both
for file in `cat html_src_files.lst | sed -e 's/#.*//g'`; do
    FILE=`echo $file | sed -e 's|^\./||' -e 's|.org$||' -e 's|.md$||'`
    URL=`grep -l "gitlabpages.*$FILE" /tmp/course/html/*.html | sed -e 's|.html$||' -e 's|/tmp/course/html/|https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/|'`
    echo "$file               #     $URL"
done
#+end_src

#+RESULTS:
#+begin_example
./module1/ressources/introduction_to_markdown_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/ecbcbd8bfafd48e9a912f66237b76c92
./module1/ressources/introduction_to_markdown.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/ecbcbd8bfafd48e9a912f66237b76c92
./module1/ressources/sequence1_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/9719a1f5b81a4f2d89c27e54ce81f498
./module1/ressources/sequence2_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/565a72fe3c8b464f8ec3e31b56cd4c57
./module1/ressources/sequence3_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/d85eb783ff884ff8b1306b680326b198
./module1/ressources/sequence5_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/c96bfaf3804847a38d2a8850ee0baf54
./module1/ressources/sequence1.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/9719a1f5b81a4f2d89c27e54ce81f498
./module1/ressources/sequence2.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/565a72fe3c8b464f8ec3e31b56cd4c57
./module2/ressources/maintaining_a_journal.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/0af0b6bf03194c1bb2798c29d7375a76
./module2/ressources/maintaining_a_journal_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/0af0b6bf03194c1bb2798c29d7375a76
./module2/ressources/jupyter.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/8dcce91be83c4ece834abfa98b8bbfb1
./module2/ressources/jupyter_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/8dcce91be83c4ece834abfa98b8bbfb1
./module2/ressources/rstudio.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/09d339f009cd4b45a468583ab7730221
./module2/ressources/rstudio_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/09d339f009cd4b45a468583ab7730221
./module2/ressources/gitlab.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/448cd6f9018545d1a67405551add6fda
./module2/ressources/gitlab_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/448cd6f9018545d1a67405551add6fda
./module2/ressources/emacs_orgmode.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/954f94ce4f9a4d858d47b41793cc96c8
./module2/ressources/emacs_orgmode_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/954f94ce4f9a4d858d47b41793cc96c8
./module2/ressources/sequence6_examples/README.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/827b36e10ddd49239f1c62cc1903951a
./module2/ressources/sequence6_examples/README_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/827b36e10ddd49239f1c62cc1903951a
./module2/exo4/stat_activity.org               #     
./module2/slides/ressources.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/36723edefc72488ab44e269501cfc9f8
./module2/slides/ressources_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/36723edefc72488ab44e269501cfc9f8
./module3/ressources/iso_date_format.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/316fc24edc1b44278ada4ca531c705be
./module3/ressources/iso_date_format_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/316fc24edc1b44278ada4ca531c705be
./module3/ressources/influenza-like-illness-analysis-orgmode+R.org               #     
./module3/ressources/influenza-like-illness-analysis-orgmode+Lisp+Python+R.org               #     
./module3/ressources/influenza-like-illness-analysis-orgmode.org               #     
./module3/ressources/analyse-syndrome-grippal-orgmode.org               #     
./module3/ressources/analyse-syndrome-grippal-orgmode+R.org               #     
./module3/ressources/analyse-syndrome-grippal-orgmode+Lisp+Python+R.org               #     
./module4/ressources/resources_refs.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/3c3c8cb2160d44e09a087b825beb92b8
./module4/ressources/resources_refs_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/3c3c8cb2160d44e09a087b825beb92b8
./module4/ressources/exo1.org               #     
./module4/ressources/exo2.org               #     
./module4/ressources/exo3.org               #     
./module4/ressources/resources_environment.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/4be922aa6b8c471ab1addcdbddb4e487
./module4/ressources/resources_environment_fr.org               #     https://www.fun-mooc.fr/courses/course-v1:inria+41016+session02/jump_to_id/4be922aa6b8c471ab1addcdbddb4e487
./documents/notebooks/notebook_RStudio_SASmarkdown.md               #     
./documents/tuto_git_gtlab/tuto_git_gitlab.md               #     
./documents/tuto_rstudio_gitlab/tuto_rstudio_gitlab.md               #     
./documents/tuto_magit/tuto_magit.md               #     
./documents/tuto_emacs_windows/tuto_emacs_windows.md               #     
./documents/tuto_jupyter_windows/tuto_jupyter_windows.md               #     
#+end_example

* Local Testing
Having to go through a commit/push all the time to check whether it
works or not is a pain. Here a simple mechanism that allows to test
the CI script locally.

#+begin_src perl :results output :file gitlab-ci.sh :exports both
open INPUT, ".gitlab-ci.yml";
my($line);
my($script_found)=0;
while(defined($line=<INPUT>)) {
    if($line=~/\s*script:/) { $script_found=1; next;}
    if($line=~/\s*artifacts:/) { $script_found=0; }
    if($script_found) {
	if($line =~ /^\s*#\s/) { next; }
#	if($line =~ /mv.*public/) { next; }
	$line =~ s/^\s*-\s//g;
	print($line);
    }
}
#+end_src

#+RESULTS:
[[file:gitlab-ci.sh]]

#+begin_src shell :results output :exports both 
chmod +x ./gitlab-ci.sh
./gitlab-ci.sh 
#+end_src

