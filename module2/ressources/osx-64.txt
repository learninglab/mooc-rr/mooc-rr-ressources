# This file may be used to create an environment using:
# $ conda create --name <env> --file <this file>
# platform: osx-64
@EXPLICIT
https://repo.anaconda.com/pkgs/r/osx-64/_r-mutex-1.0.0-anacondar_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/bzip2-1.0.6-h1de35cc_5.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/ca-certificates-2019.1.23-0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/compiler-rt-4.0.1-hcfea43d_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/fribidi-1.0.5-h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/gsl-2.4-h1de35cc_4.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/jpeg-9c-h1de35cc_1001.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libcxxabi-4.0.1-hcfea43d_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libgfortran-3.0.1-h93005f0_2.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libiconv-1.15-hdd342a3_7.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libsodium-1.0.16-h3efe00b_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/llvm-openmp-4.0.1-hcfea43d_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/make-4.2.1-h3efe00b_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pandoc-2.2.3.2-0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pixman-0.38.0-h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pthread-stubs-0.3-hdd91f34_1.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-kbproto-1.0.7-h1de35cc_1002.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-libice-1.0.9-h01d97ff_1004.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-libxau-1.0.9-h1de35cc_0.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-libxdmcp-1.1.3-h01d97ff_0.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-renderproto-0.11.1-h1de35cc_1002.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-xextproto-7.3.0-h1de35cc_1002.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-xproto-7.0.31-h1de35cc_1007.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/xz-5.2.4-h1de35cc_4.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/yaml-0.1.7-hc338f04_2.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/zlib-1.2.11-h1de35cc_3.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/gfortran_osx-64-4.8.5-h22b1bf0_8.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libcxx-4.0.1-hcfea43d_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libgcc-4.8.5-hdbeacc1_10.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libopenblas-0.3.3-hdc02c5d_3.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libpng-1.6.37-ha441bb4_0.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/libxcb-1.13-h1de35cc_1002.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/openblas-0.3.6-hd44dcd8_3.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/openssl-1.0.2r-h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/tk-8.6.8-ha441bb4_0.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-libsm-1.2.3-h01d97ff_1000.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/zstd-1.3.7-h5bba6e5_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/bwidget-1.9.11-1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/cctools-895-1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/freetype-2.9.1-hb4e5f40_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/gmp-6.1.2-hb37e062_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/graphite2-1.3.13-h2098e52_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/hdf5-1.10.2-hfa1e0ec_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/icu-58.2-h4b95b61_1.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/libblas-3.8.0-10_openblas.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libffi-3.2.1-h475c297_4.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libprotobuf-3.7.1-hd9629dc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libssh2-1.8.0-h322a93b_4.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libtiff-4.0.10-hcb84e12_2.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/llvm-lto-tapi-4.0.1-1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/ncurses-6.1-h0a44026_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pcre-8.43-h0a44026_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/tktable-2.10-h1de35cc_0.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-libx11-1.6.7-h1de35cc_1000.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/zeromq-4.2.5-h0a44026_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/gettext-0.19.8.1-h15daf44_3.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/ld64-274.2-1.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/libcblas-3.8.0-10_openblas.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libcurl-7.61.1-hf30b1f0_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libedit-3.1.20181209-hb402a30_0.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/liblapack-3.8.0-10_openblas.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/libxml2-2.9.9-hab757c2_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/mpfr-4.0.1-h3018a27_3.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/readline-7.0-hc1231fa_4.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-libxext-1.3.4-h01d97ff_0.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/xorg-libxrender-0.9.10-h01d97ff_1002.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/curl-7.61.1-ha441bb4_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/fontconfig-2.13.0-h5d5b041_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/glib-2.56.2-hd9629dc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/krb5-1.16.1-h24a3359_6.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/liblapacke-3.8.0-10_openblas.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/llvm-4.0.1-1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/mpc-1.1.0-h6ef4df4_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/sqlite-3.23.1-hf1716c9_0.tar.bz2
https://conda.anaconda.org/conda-forge/osx-64/blas-2.10-openblas.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/cairo-1.14.12-hc4e6be7_4.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/clang-4.0.1-1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/python-3.6.5-hc167b69_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/appnope-0.1.0-py36hf537a9a_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/attrs-19.1.0-py36_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/backcall-0.1.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/backports-1.0-py36_1.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/backports_abc-0.5-py_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/beautifulsoup4-4.6.3-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/certifi-2019.3.9-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/clang_osx-64-4.0.1-h1ce6c1d_16.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/clangxx-4.0.1-1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/cloudpickle-0.5.6-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/dask-core-1.2.2-py_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/decorator-4.4.0-py36_1.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/defusedxml-0.6.0-py_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/dill-0.2.9-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/entrypoints-0.3-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/fastcache-1.1.0-py36h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/free/osx-64/funcsigs-1.0.2-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/gmpy2-2.0.8-py36h6ef4df4_2.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/harfbuzz-1.8.8-hb8d4a28_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/ipython_genutils-0.2.0-py36h241746c_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/kiwisolver-1.1.0-py36h0a44026_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/llvmlite-0.23.2-py36hc454e04_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/markupsafe-1.1.1-py36h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/mistune-0.8.4-py36h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/mpmath-1.1.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/numpy-base-1.16.3-py36ha711998_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/olefile-0.46-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pandocfilters-1.4.2-py36_1.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/parso-0.4.0-py_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pickleshare-0.7.5-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/prometheus_client-0.6.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/ptyprocess-0.6.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/pyparsing-2.4.0-py_0.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/pytz-2019.1-py_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pyyaml-5.1-py36h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pyzmq-17.1.2-py36h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/free/osx-64/scandir-1.5-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/send2trash-1.5.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/simplegeneric-0.8.1-py36_2.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/six-1.12.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/sqlalchemy-1.2.18-py36h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/testpath-0.4.2-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/toolz-0.9.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/tornado-6.0.2-py36h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/wcwidth-0.1.7-py36h8c6ec74_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/webencodings-0.5.1-py36_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/xlrd-1.2.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/backports.shutil_get_terminal_size-1.0.0-py36_2.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/clangxx_osx-64-4.0.1-h22b1bf0_16.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/configparser-3.7.4-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/cycler-0.10.0-py36hfc81398_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/cytoolz-0.9.0.1-py36h1de35cc_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/jedi-0.13.3-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/numpy-1.16.3-py36h926163e_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/packaging-19.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pango-1.42.4-h060686c_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pathlib2-2.3.3-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pexpect-4.7.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pillow-6.0.0-py36hb68e598_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/protobuf-3.7.1-py36h0a44026_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pyrsistent-0.14.11-py36h1de35cc_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/python-dateutil-2.8.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/setuptools-41.0.1-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/singledispatch-3.4.0.3-py36hf20db9d_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/sympy-1.1.1-py36h7f3cf04_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/terminado-0.8.2-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/traitlets-4.3.2-py36h65bd3ce_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/bleach-3.1.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/cython-0.28.5-py36h0a44026_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/h5py-2.7.1-py36ha8ecd60_2.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/imageio-2.5.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/jinja2-2.10.1-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/jsonschema-3.0.1-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/jupyter_core-4.4.0-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/matplotlib-2.2.3-py36h54f8f79_0.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/networkx-2.3-py_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/numba-0.38.1-py36h6440ff4_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/numexpr-2.6.9-py36hafae301_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pandas-0.22.0-py36h0a44026_0.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/pygments-2.4.0-py_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pywavelets-1.0.3-py36h1d22016_1.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-base-3.5.1-h539fb6c_1.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/scipy-1.1.0-py36h1a1e112_2.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/wheel-0.33.4-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/bokeh-0.12.16-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/jupyter_client-5.2.4-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/nbformat-4.4.0-py36h827af21_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/patsy-0.5.1-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/pip-19.1.1-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/prompt_toolkit-2.0.9-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-abind-1.4_5-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-assertthat-0.2.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-backports-1.1.2-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-base64enc-0.1_3-r351h6402f54_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-bh-1.66.0_1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-bindr-0.1.1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-bit-1.1_14-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-bitops-1.0_6-r351h6402f54_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-cardata-3.0_1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-codetools-0.2_15-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-colorspace-1.3_2-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-crayon-1.3.4-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-curl-3.2-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-data.table-1.11.4-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-dbi-1.0.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-deoptimr-1.0_8-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-dichromat-2.0_0-r351hf348343_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-digest-0.6.15-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-fansi-0.2.3-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-foreign-0.8_71-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-fracdiff-1.4_2-r351h6402f54_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-git2r-0.23.0-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-glue-1.3.0-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-gower-0.1.2-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-gtable-0.2.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-highr-0.7-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-iterators-1.0.10-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-jsonlite-1.5-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-kernlab-0.9_26-r351h46e27c5_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-kernsmooth-2.23_15-r351h0b560c1_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-labeling-0.3-r351hf348343_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-lattice-0.20_35-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-lazyeval-0.2.1-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-magrittr-1.5-r351hf348343_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-mass-7.3_50-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-mime-0.5-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-mnormt-1.5_5-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-nloptr-1.0.4-r351h32998d9_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-nnet-7.3_12-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-numderiv-2016.8_1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-openssl-1.0.2-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-pbdzmq-0.3_3-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-pkgconfig-2.0.1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-plogr-0.2.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-pls-2.6_0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-quadprog-1.5_5-r351h0b560c1_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-r6-2.2.2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-randomforest-4.6_14-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rappdirs-0.3.1-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rcolorbrewer-1.1_2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rcpp-0.12.18-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rematch-1.0.1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rlang-0.2.1-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rpart-4.1_13-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rstudioapi-0.7-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-sfsmisc-1.1_2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-sourcetools-0.1.7-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-sparsem-1.77-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-squarem-2017.10_1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-stringi-1.2.4-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-timedate-3043.102-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-utf8-1.1.4-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-uuid-0.1_2-r351h6402f54_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-viridislite-0.3.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-whisker-0.3_2-r351hf348343_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-withr-2.1.2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-xtable-1.8_2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-yaml-2.2.0-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-zip-1.0.0-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/scikit-image-0.14.2-py36h0a44026_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/scikit-learn-0.19.2-py36hebd9d1a_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/ipython-7.5.0-py36h39e3cac_0.tar.bz2
https://repo.anaconda.com/pkgs/main/noarch/nbconvert-5.5.0-py_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-bindrcpp-0.2.2-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-bit64-0.9_7-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-class-7.3_14-r351h6402f54_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-cli-1.0.0-r351h6115d3f_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-config-0.3-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-foreach-1.4.4-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-hexbin-1.27.2-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-hms-0.4.2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-htmltools-0.3.6-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-httr-1.3.1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-later-0.7.3-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-magic-1.5_8-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-markdown-0.8-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-matrix-1.2_14-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-memoise-1.1.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-minqa-1.2.4-r351h4496799_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-modelmetrics-1.1.0-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-munsell-0.5.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-nlme-3.1_137-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-openxlsx-4.1.0-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-plyr-1.8.4-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-prettyunits-1.0.2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rcpparmadillo-0.8.600.0.0-r351h649bfe0_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rcpproll-0.3.0-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rcurl-1.95_4.11-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-robustbase-0.93_2-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rprojroot-1.3_2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-sp-1.3_1-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-stringr-1.3.1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-xml2-1.2.0-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-zoo-1.8_3-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/statsmodels-0.9.0-py36h1d22016_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/ipykernel-5.1.0-py36h39e3cac_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-blob-1.1.1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-cvst-0.2_2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-devtools-1.13.6-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-evaluate-0.11-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-geometry-0.3_6-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-lmtest-0.9_36-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-lubridate-1.7.4-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-maptools-0.9_3-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-matrixmodels-0.4_1-r351hf348343_4.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-mgcv-1.8_24-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-pillar-1.3.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-promises-1.0.1-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-psych-1.8.4-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rcppeigen-0.3.3.4.0-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-repr-0.15.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-reshape2-1.4.3-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-scales-0.5.0-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-selectr-0.4_1-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-survival-2.42_6-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-xts-0.11_0-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/seaborn-0.8.1-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/notebook-5.7.8-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-ddalpha-1.3.4-r351h4496799_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-drr-0.0.3-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-httpuv-1.4.5-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-irdisplay-0.5.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-knitr-1.20-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-lava-1.6.2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-lme4-1.1_17-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-quantreg-5.36-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rsqlite-2.1.1-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rvest-0.3.2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-tibble-1.4.2-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-ttr-0.23_3-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-cellranger-1.1.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-dimred-0.1.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-forcats-0.3.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-ggplot2-3.0.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-irkernel-0.8.12-r351_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-pbkrtest-0.4_7-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-prodlim-2018.04.18-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-purrr-0.2.5-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-quantmod-0.4_13-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-readr-1.1.1-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/widgetsnbextension-3.4.2-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/main/osx-64/ipywidgets-7.2.1-py36_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-haven-1.1.2-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-ipred-0.9_6-r351h6402f54_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-readxl-1.1.0-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-tidyselect-0.2.4-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-tseries-0.10_45-r351h0b560c1_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-dplyr-0.7.6-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-rio-0.5.10-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-car-3.0_0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-dbplyr-1.2.2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-tidyr-0.8.1-r351h32998d9_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-broom-0.5.0-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/rpy2-2.9.4-py36r351h1d22016_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-modelr-0.1.2-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-recipes-0.1.3-r351hf348343_0.tar.bz2
https://repo.anaconda.com/pkgs/r/osx-64/r-caret-6.0_80-r351h6402f54_0.tar.bz2
