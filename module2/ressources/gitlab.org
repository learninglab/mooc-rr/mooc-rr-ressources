# -*- mode: org -*-
#+TITLE:     Git and GitLab
#+DATE: June, 2018
#+STARTUP: overview indent
#+OPTIONS: num:nil toc:t
#+PROPERTY: header-args :eval never-export

*This document is particularly important if you follow the RStudio or
the Org-Mode path.* *If you follow the Jupyter path, it can be
ignored at first* *as we have closely integrated Jupyter and
GitLab in the context of this MOOC.*

So far, you only used git via the web interface from the GitLab we
deployed for the MOOC: https://app-learninglab.inria.fr/gitlab/

If you access this link from the FUN platform, you do not have to
authenticate and you can readily read and modify all your files.  This
is very convenient but in most cases, you will want to have your own
local copy of the repository and you will have to synchronize your
local copy with the remote GitLab one. To propagate your
modifications, you will obviously have to authenticate yourself on
GitLab. 

This document describes the software you need to have installed on
your machine and how to handle authentication.  The "Configuring Git"
section is illustrated in a [[https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/jump_to_id/7508aece244548349424dfd61ee3ba85][video tutorial]] (in French).

Please read all these instructions carefully, in particular the one on
"Configuring your password on GitLab".
* Table of Contents                                                     :TOC:
- [[#installing-git][Installing Git]]
  - [[#linux-debian-ubuntu][Linux (Debian, Ubuntu)]]
  - [[#mac-osx-and-windows][Mac OSX and Windows]]
- [[#configuring-git][Configuring Git]]
  - [[#telling-git-who-you-are-name-and-email][Telling Git who you are: Name and Email]]
  - [[#dealing-with-proxies][Dealing with proxies]]
  - [[#getting-your-default-password-on-gitlab-and-possibly-changing-it][Getting your default password on GitLab (and possibly changing it)]]
  - [[#remembering-your-password-locally][Remembering your password locally]]
  - [[#optional-authenticating-through-ssh][Optional: authenticating through SSH]]
- [[#using-git-through-the-command-line-to-synchronize-your-local-files-with-gitlab][Using Git through the command line to synchronize your local files with Gitlab]]

* Installing Git
** Linux (Debian, Ubuntu)
We provide here only instructions for Debian-based distributions. Feel
free to contribute to this document to provide up-to-date information
for other distributions (e.g., RedHat, Fedora).

Run (as root):
#+begin_src sh :results output :exports both
apt-get update ; apt-get install git
#+end_src
** Mac OSX and Windows
- Download and install Git from the [[https://git-scm.com/downloads][Git website]].
- Optional Git clients (should not be needed if you work within
  RStudio):
  - [[https://www.sourcetreeapp.com/][SourceTree]]
  - [[https://desktop.github.com/][GitHub Desktop]] 
    #+BEGIN_QUOTE
    [[https://github.com/desktop/desktop/issues/852][Apparently]], this works with GitLab and https.
    #+END_QUOTE
* Configuring Git
** Telling Git who you are: Name and Email
1. Open terminal.
2. Set a Git username and email:
   #+begin_src shell :results output :exports both
   git config --global user.name "Mona Lisa"
   git config --global user.email "email@example.com"
   #+end_src

   These two steps are really important to commit. If you forget to do
   so, you will get the following message:
   #+BEGIN_CENTER
   [[file:gitlab_images/commit3.png]]
   #+END_CENTER

3. Confirm that you have set the Git username correctly:   
   #+begin_src shell :results output :exports both
   git config --global user.name
   git config --global user.email
   #+end_src

   #+RESULTS:
   : Mona Lisa
   : email@example.com
** Dealing with proxies
You may be behind a proxy, in which case you may have trouble cloning
or fetching from a remote repository or you may get an error like
=unable to access ... Couldn't resolve host ...=

In such case, consider something like this:
#+begin_src shell :results output :exports both
git config --global http.proxy http://proxyUsername:proxyPassword@proxy.server.com:port
#+end_src

The =proxyPassword= will be stored in plain text (unencrypted) in your ~.gitconfig~ file,
which you may not want. In that case, remove it from the URL and you
will be prompted for it every time it is needed.

To stop using a proxy, simply use the following command:
#+begin_src shell :results output :exports both
git config --global --unset http.proxy
#+end_src

** Getting your default password on GitLab (and possibly changing it)
*Warning (Jupyter users) :* changing your default Gitlab password will
prevent you from committing in Jupyter. You will have to do the extra
step of changing your =~/.git-credentials= in the Jupyter environment
(possibly several times).

1. Get your default password using the [[https://app-learninglab.inria.fr/moocrr/jupyter/services/password][Gitlab credentials retrieval
   tool]] as described on the [[https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/jump_to_id/7508aece244548349424dfd61ee3ba85][corresponding resource]].
   #+BEGIN_CENTER
   [[file:gitlab_images/password_retrieval.png]]
   #+END_CENTER
   The first long and ugly character sequence is your GitLab
   login/id. It is easy to find once you are logged on gitlab. The
   second one however is your password and this webpage is the only
   place where you can find it. We used the FUN authentification
   mechanism to propagate your credentials so only you can have access
   to it. You'll need to use this password when trying to propagate
   some modifications from your computer to GitLab.

   /Note: You have to access this webpage from the FUN platform
   otherwise you may get a 405 error/ /when trying to direcly open
   [[https://app-learninglab.inria.fr/jupyterhub/services/password]]./
   #+BEGIN_CENTER
   [[file:gitlab_images/erreur405.png]]
   #+END_CENTER
2. Access [[https://www.fun-mooc.fr/courses/course-v1:inria+41016+self-paced/jump_to_id/5571950188c946e790f06d4bc90fb5f6][GitLab]] from the FUN plateform (click on "Accédez à Gitlab / Access to Gitlab" Button). 

   /Note: Again, you have to access Gitlab from the FUN platform
   otherwise you may get a 405 error/ /when trying to direcly open
   https://app-learninglab.inria.fr/gitlab/users/sign_in/.
3. Click on the first =Sign in= button (alternatively, you can the
   login/password you just retrieved and use the second =Sign in=
   button).
   #+BEGIN_CENTER
   [[file:gitlab_images/signin.png]]
   #+END_CENTER
4. If you wish to modify your password, you should go to =Account > Settings > Password= 
   and define your password using the default
   password you just retrieved. Again, if you use the Jupyter
   notebooks we have deployed for the MOOC, remember that changing
   your default Gitlab password will prevent you from committing in
   Jupyter. You will have to do the extra step of changing your
   Jupyter =~/.git-credentials= through a Jupyter console (see next
   section).
   #+BEGIN_CENTER
   [[file:gitlab_images/password.png]]
   #+END_CENTER
** Remembering your password locally
If you clone your repository by simply pasting the GitLab URL, you will be
prompted for your login and your password every time you want to
propagate your local modifications, which is tedious. This is why you
should ask git to remember your login and password as follows
#+begin_src shell :results output :exports both
git config --global credential.helper cache                  # remember my password
git config --global credential.helper "cache --timeout=3600" # for one hour at most
#+end_src
With this setup, you will be prompted for your password but it will be
cached in memory and they will not be asked again before an hour.  You
may want to read [[https://stackoverflow.com/questions/5343068/is-there-a-way-to-skip-password-typing-when-using-https-on-github][these instructions]] to better understand how all this
works.

If you want your password to be permanently remembered, you should use
this command
#+begin_src shell :results output :exports both
git config credential.helper store
#+end_src
Your password will be then stored in a =.git-credentials= file in plain
text. On a perfectly secured machine, it may be fine... or not... ;)
Use it at your own risk.

To delete all the passwords that may have been stored, simply use the
following command:
#+begin_src shell :results output :exports both
git config --system --unset credential.helper
#+end_src

Finally, those of you using Windows may get the following error
message:
#+BEGIN_EXAMPLE
git: 'credential-cache' is not a git command. See 'get --help'.
#+END_EXAMPLE
This issue is mentioned on [[https://stackoverflow.com/questions/11693074/git-credential-cache-is-not-a-git-command][stackoverflow]]. In such case, try the
following command:
#+begin_src shell :results output :exports both
git config --global --unset credential.helper
#+end_src
If the problem persists, do not hesitate to describe your problem on
the MOOC forum to get some help.
** Optional: authenticating through SSH
There are two ways of authenticating and synchronizing your local
repository with GitLab: through HTTPS or through SSH. The first one is
what was just described and does not require any particular software
installation on your machine so this is what I recommend for this
MOOC. Yet, I like the second one better (although is may seem a bit
more technical), which is why I describe it here. It consists in
installing SSH, creating a pair or private/public keys, and uploading
your SSH public key on GitLab. This section provides with information
on how to do this.
*** Installing SSH
**** Linux (Debian, Ubuntu)
We provide here only instructions for debian-based distributions. Feel
free to contribute to this document to provide up-to-date information
for other distributions (e.g., RedHat, Fedora).

Run (as root):
#+begin_src sh :results output :exports both
apt-get update ; apt-get install openssh-client
#+end_src
**** macOS
You do not have anything to do as it is installed by default.
**** Windows
You should install the [[https://www.ssh.com/ssh/putty/windows/][Putty]] client. Once it is installed, look for
the section on [[https://www.ssh.com/ssh/putty/windows/puttygen][generating an SSH key]].
*** Setting up SSH on GitLab
Here are [[https://docs.gitlab.com/ee/ssh/][all the official explanations on how to set up your SSH key
on GitLab]]. Alternatively, you may also want to have a look at this
video:
#+BEGIN_EXPORT html
<iframe width="560" height="315" src="https://www.youtube.com/embed/54mxyLo3Mqk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
#+END_EXPORT
* Using Git through the command line to synchronize your local files with Gitlab
This section describes a generic (through the command line) way to
synchronize your local files with Gitlab. You will not need this if
you follow the Jupyter path. If you follow the RStudio path, all
these operations can be done through RStudio and you may want to read
[[/jump_to_id/1a4f58a1efed437c93a9f5c5f15df428][the corresponding instructions]]. If you follow the Org-Mode path, all
these operations can be done through Magit and you may want to read
[[/jump_to_id/508299f7373449a3939faa5b11462bc4][the corresponding instructions]]. 

Here are other ways to learn Git through the command line:
- The [[http://swcarpentry.github.io/git-novice/][Software Carpentry git tutorial]]
- The (freely available) Pro Git book ([[https://git-scm.com/book/en/v2][in English]] or [[https://git-scm.com/book/fr/v2][in
  French]]). Reading the first two chapters is enough to get a good start.
- [[https://learngitbranching.js.org/][Learn Git Branching]] will allow to interactively learn Git and to 
  understand with branches.

Now, let's start!
1. Obtain the repository URL
   #+BEGIN_CENTER
   [[file:rstudio_images/adresse_depot_v2.png]]
   #+END_CENTER
2. Cloning the repository
   #+begin_src shell :results output :exports both
   cd /the/directory/where/you/want/to/clone/your/repository
   git clone https://app-learninglab.inria.fr/gitlab/xxx/mooc-rr.git
   #+end_src
   Alternatively, you may want to indicate now your login although I
   rather suggest you follow the /Remembering your password locally/
   instructions:
   #+begin_src shell :results output :exports both
   git clone https://xxx@app-learninglab.inria.fr/gitlab/xxx/mooc-rr.git
   #+end_src
   Now a directory =mooc-rr= has been created on your computer.
3. Inspect the repository
   #+begin_src shell :results output :exports both
   cd mooc-rr
   ls # (Unix)
   dir # (Windows)
   #+end_src
4. Synchronizing to GitLab

   You should indicate which files to track (=git add=) and commit them
   locally (=git commit=) before they can be transfered (=git push=) to
   GitLab. The =git status= will indicate you whether files are
   tracked/modified/committed/...

   Let's assume you just created a =fichier.txt= file on the top of
   the =mooc-rr= directory.

   #+begin_src shell :results output :exports both
   git status
   #+end_src

   #+BEGIN_CENTER
   [[file:gitlab_images/status1.png]]
   #+END_CENTER
   
   #+begin_src shell :results output :exports both
   git add fichier.txt
   git status
   #+end_src

   #+BEGIN_CENTER
   [[file:gitlab_images/status2.png]]
   #+END_CENTER
   
   #+begin_src shell :results output :exports both
   git commit -m "message commit"
   #+end_src

   #+BEGIN_CENTER
   [[file:gitlab_images/commit_git.png]]
   #+END_CENTER
   
   #+begin_src shell :results output :exports both
   git status
   #+end_src

   #+BEGIN_CENTER
   [[file:gitlab_images/status3.png]]
   #+END_CENTER
   
   The file can then be transfered to GitLab:
   #+begin_src shell :results output :exports both
   git push
   #+end_src

   At this point, git will ask you about your login/password unless
   you followed the previous /Remembering your password locally/ instructions.

   N.B.: you will not be allowed to propagate your modifications to
   GitLab if other modifications (e.g., from someone else) have been
   propagated in between

   #+BEGIN_CENTER
   [[file:gitlab_images/rejected.png]]
   #+END_CENTER
5. Synchronizing from Gitlab: to avoid the previous problem, you need
   to fetch the remote GitLab modifications first and apply them
   locally.
   #+begin_src shell :results output :exports both
   git pull
   #+end_src
   Only then will you be able to =git push=.


